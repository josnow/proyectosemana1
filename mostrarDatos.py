def mostrarResultadoAUsuario(usuarioAMostrar):
    print("--------------------------------------------")
    print("Hola " + usuarioAMostrar["nombre"] + " " + usuarioAMostrar["apellido"])
    print("Tu nivel de prioridad para la vacunación es: " + str(usuarioAMostrar["prioridad"]))
    print("El centro medico asignado es: " + usuarioAMostrar["centroMedico"])
    print("Ubicación: " + usuarioAMostrar["ubicacion"])

def mostrarRegistros(listaUsuarios):
    print("--------------------------------------------\nUsuarios agendados\n--------------------------------------------")
    for usuario in listaUsuarios:
        mostrarResultadoAUsuario(usuario)
        print("--------------------------------------------")
