import random
import json

centrosMedicos = {}

def obetenerCentrosMedicos():

    # Opening JSON file
    f = open('recursos/centrosMedicos.json',)

    # returns JSON object as a dictionary
    global centrosMedicos
    centrosMedicos = json.load(f)

    # Closing file
    f.close()


def obtenerCentroV(ubicacion):
    global centrosMedicos
    if ubicacion in centrosMedicos:
        centro = random.choice(centrosMedicos[ubicacion])
        return centro

obetenerCentrosMedicos()