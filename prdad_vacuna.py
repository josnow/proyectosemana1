import obtenerCentroV

def prioridad_vacunacion(usuario):

    edad = usuario["edad"]
    ubicacion = usuario["ubicacion"]
    # texto = """Hola {nomb} {apell}
    # tu nivel de prioridad para la vacunación es: {prioridad} , y tu centro de vacunación asignado es: {centro}
    #              """  # placeholder

    if edad >= 61 and (ubicacion.lower() == "sur" or ubicacion.lower() == "norte"):
        usuario["centroMedico"] = obtenerCentroV.obtenerCentroV(ubicacion.lower())
        usuario["prioridad"] = 1
        return usuario

    elif edad >= 61 and (ubicacion.lower() == "oriente" or ubicacion.lower() == "occidente"):
        usuario["centroMedico"] = obtenerCentroV.obtenerCentroV(ubicacion.lower())
        usuario["prioridad"] = 2
        return usuario

    elif edad >= 41 and edad <= 60 and (ubicacion.lower() == "sur" or ubicacion.lower() == "norte"):
        usuario["centroMedico"] = obtenerCentroV.obtenerCentroV(ubicacion.lower())
        usuario["prioridad"] = 3
        return usuario

    elif edad >= 41 and edad <= 60 and (ubicacion.lower() == "oriente" or ubicacion.lower() == "occidente"):
        usuario["centroMedico"] = obtenerCentroV.obtenerCentroV(ubicacion.lower())
        usuario["prioridad"] = 4
        return usuario

    elif edad >= 21 and edad <= 40 and (ubicacion.lower() == "sur" or ubicacion.lower() == "norte"):
        usuario["centroMedico"] = obtenerCentroV.obtenerCentroV(ubicacion.lower())
        usuario["prioridad"] = 5
        return usuario

    elif edad >= 21 and edad <= 40 and (ubicacion.lower() == "oriente" or ubicacion.lower() == "occidente"):
        usuario["centroMedico"] = obtenerCentroV.obtenerCentroV(ubicacion.lower())
        usuario["prioridad"] = 6
        return usuario
        
    elif edad >= 0 and edad <= 20 and (ubicacion.lower() == "sur" or ubicacion.lower() == "norte"):
        usuario["centroMedico"] = obtenerCentroV.obtenerCentroV(ubicacion.lower())
        usuario["prioridad"] = 7
        return usuario

    elif edad >= 0 and edad <= 20 and (ubicacion.lower() == "oriente" or ubicacion.lower() == "occidente"):
        usuario["centroMedico"] = obtenerCentroV.obtenerCentroV(ubicacion.lower())
        usuario["prioridad"] = 8
        return usuario

    else:
        return None

# # Probar la función:


# print(prioridad_vacunacion(usuario))
