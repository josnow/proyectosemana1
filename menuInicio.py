def pedirDatosAlUsuario():
    usuario = {}
    usuario["nombre"] = input("Por favor ingrese su nombre: ")
    usuario["apellido"] = input("Por favor ingrese su apellido: ")
    usuario["edad"] = int(input("Por favor ingrese su edad: "))
    usuario["ubicacion"] = input("Por favor ingrese su ubicación: ")
    return usuario

def menuInicio():
    print("--------------------------------------------------")
    print("Menu de opciones")
    print("1. Consultar mi prioridad y centro de vacunacion.")
    print("2. Ver el registro de los usuarios con centro de vacunacion asignado.")
    print("0. Salir")
    seleccionUsuario = input("Digite una opción: ")
    return seleccionUsuario