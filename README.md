# README #

Proyecto de la semana 1, P52, Ciclo 3, Mision TIC 2022 

# Equipo 2 #
* Stephanie Avila
* Adriana Bohorquez
* Anna Gabriela Salazar
* William  I. Jimenez
* Josue Nuñez

[Proyecto en Jira](https://proyecto-josu.atlassian.net/jira/software/projects/E2P/boards/2)
### Descripcion del programa ###

Este programa le permite a un usuario ingresar sus datos (Nombre, Apellido, Edad y Ubicación (entre 4 posibles respuestas)).

El usuario tendrá la opción de visualizar el punto de vacunacion asignado, también se podrá ver todos los usuarios consignados con punto de vacunacion y datos registrados. El criterio de asignacion de punto de vacunacion esta dado por la ubicacion proporcionada por el usuario en el momento del ingreso de datos.

### Uso del programa ###

* La rama de desarrollo contiene 6 archivos .py que tienen el código necesario para que corra el programa, el archivo vacuna.py condensa todo el código por medio del llamado a los demás archivos. 
* La carpeta recursos contiene el archivo centrosMedicos.json que funciona en conjunto con el archivo obtenerCentroV.py
* Las importaciones necesarias para que el programa corra son: 
    * json 
    * random

* Inicialmente el programa solicita al usuario que ingrese los datos de:
    * Nombre
    * Apellido
    * Edad
    * Ubicación (debe indicar entre: sur, norte, oriente, occidente)
* Posteriormente le presenta un menu de opciones para seleccionar 1 de 3 que le permitiran:

    0. Salir del programa
    1. Consultar la prioridad y centro de vacunación
    2. Ver el registro de los usuarios con el centro de vacunación asignado
    
* El programa empleará el código de dos archivos .py para traer un centro de vacunación aleatorio con base a la ubicación y designar la prioridad según la edad del usuario
* Finalmente se almacenará la información del usuario en un diccionario que podrá ser consultado con la opción 2. o presentar la información de la opción 1.
