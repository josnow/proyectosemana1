
import menuInicio
import prdad_vacuna
import mostrarDatos
import registroDeUsuario

def main():
    seleccion = -1
    while seleccion != 0:
        seleccion = int(menuInicio.menuInicio())
        if seleccion == 1:
            usuario = menuInicio.pedirDatosAlUsuario() # Pedir datos al usuario y transformarlos en un diccionario
            usuario = prdad_vacuna.prioridad_vacunacion(usuario) # Obtener usuario con datos resultantes
            mostrarDatos.mostrarResultadoAUsuario(usuario) # Mostrar resultados al usuario
            registroDeUsuario.registrousuario(usuario) # Guardar este registro
        elif seleccion == 2:
            registroDeUsuario.mostrarRegistro()
            
main()
